#include "pcm/pcm.hpp"

#include <iostream>
#include <numeric>

#include <Eigen/Core>

#include "gtest/gtest.h"

TEST(PCMTest, Test1) {
  class Test1ConsistencyEvaluator : public PCM::ConsistencyEvaluator
  {
    Eigen::MatrixXi data;
    
   public:
    Test1ConsistencyEvaluator()
    {
      Eigen::MatrixXi data_init(6,6);
      data_init <<
          1, 1, 1, 1, 0, 1,
          1, 1, 1, 0, 0, 0,
          1, 1, 1, 1, 1, 1,
          1, 0, 1, 1, 0, 1,
          0, 0, 1, 0, 1, 0,
          1, 0, 1, 1, 0, 1;

      data = data_init;
    }
    
    bool evaluate_consistency(int i, int j)
    {
      return this->data(i,j);
    }
  };

  PCM::PattabiramanMaxCliqueSolverExact solver;
  Test1ConsistencyEvaluator ce;

  PCM::PCMSolver<PCM::PattabiramanMaxCliqueSolverExact,
                 Test1ConsistencyEvaluator>
      pcm(solver, ce);
  pcm.add_measurements(6);

  auto consistent_set = pcm.solve_pcm();

  int consistent_set_size = 0;
  for (const auto& b : consistent_set)
  {
    if(b)
    {
      consistent_set_size ++;
    }
  }
  
  ASSERT_TRUE(consistent_set_size == 4);

  std::vector<bool> true_map = {true, false, true, true, false, true};
  for(int i=0;i<6;i++)
    ASSERT_TRUE(consistent_set[i] == true_map[i]) << "Returned Consistent Set Doesn't Match True Map";
}

TEST(PCMTest, Test2) {
  class Test1ConsistencyEvaluator : public PCM::ConsistencyEvaluator
  {
    Eigen::MatrixXi data;
    
   public:
    Test1ConsistencyEvaluator()
    {
      Eigen::MatrixXi data_init(6,6);
      data_init <<
          1, 1, 1, 1, 0, 1,
          1, 1, 1, 0, 0, 0,
          1, 1, 1, 1, 1, 1,
          1, 0, 1, 1, 0, 1,
          0, 0, 1, 0, 1, 0,
          1, 0, 1, 1, 0, 1;

      data = data_init;
    }
    
    bool evaluate_consistency(int i, int j)
    {
      return this->data(i,j);
    }
  };

  // Setup PCM
  PCM::PattabiramanMaxCliqueSolverExact solver;
  Test1ConsistencyEvaluator ce;

  PCM::PCMSolver<PCM::PattabiramanMaxCliqueSolverExact,
                 Test1ConsistencyEvaluator>
      pcm(solver, ce);

  // Add First three measurements
  pcm.add_measurements(3);

  auto consistent_set = pcm.solve_pcm();

  int consistent_set_size = 0;
  for (const auto& b : consistent_set)
  {
    if(b)
    {
      consistent_set_size ++;
    }
  }

  ASSERT_TRUE(consistent_set_size == 3);

  std::vector<bool> true_map = {true, true, true};
  for(int i=0;i<3;i++)
    ASSERT_TRUE(consistent_set[i] == true_map[i]) << "Returned Consistent Set Doesn't Match True Map";

  // Add Next three measurements
  pcm.add_measurements(3);

  consistent_set = pcm.solve_pcm();

  consistent_set_size = 0;
  for (const auto& b : consistent_set)
  {
    if(b)
    {
      consistent_set_size ++;
    }
  }
  
  ASSERT_TRUE(consistent_set_size == 4);

  std::vector<bool> true_map2 = {true, false, true, true, false, true};
  for(int i=0;i<6;i++)
    ASSERT_TRUE(consistent_set[i] == true_map2[i]) << "Returned Consistent Set Doesn't Match True Map";
}
