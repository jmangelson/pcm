# - Try to find libisam
# Once done this will define
#  ISAM_FOUND - System has isam
#  ISAM_INCLUDE_DIRS - The isam include directories
#  ISAM_LIBRARIES - The libraries needed to use isam
#  ISAM_DEFINITIONS - Compiler switches required for using isam

MACRO(ISAM_REPORT_NOT_FOUND REASON_MSG)
  UNSET(ISAM_FOUND)
  UNSET(ISAM_INCLUDE_DIR)
  UNSET(ISAM_LIBRARY)

  MESSAGE(FATAL_ERROR "Failed to find ISAM - " ${REASON_MSG} ${ARGN})
ENDMACRO(ISAM_REPORT_NOT_FOUND)

include(LibFindMacros)

# Dependencies
#libfind_package(Isam Suitesparse)

find_path(ISAM_INCLUDE_DIR isam/isam.h
  HINTS
  /usr
  /usr/local
  /usr/local/include
  $ENV{HOME}
  $ENV{HOME}/bin
  $ENV{HOME}/bin/include
  PATH_SUFFIXES
  isam
  isam_v1_7
  include)

find_library(ISAM_LIBRARY NAMES isam libisam
  HINTS
  /usr
  /usr/local
  /usr/lib
  /usr/local/lib
  $ENV{HOME}
  $ENV{HOME}/bin
  $ENV{HOME}/bin/lib
  PATH_SUFFIXES
  isam
  isam_v1_7
  lib)


# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(ISAM_PROCESS_INCLUDES ISAM_INCLUDE_DIR SUITESPARSE_INCLUDE_DIRS)
set(ISAM_PROCESS_LIBS ISAM_LIBRARY SUITESPARSE_LIBRARIES)
libfind_process(Isam)

IF (EXISTS ${ISAM_INCLUDE_DIR})
  MESSAGE("Found ISAM Include in: ${ISAM_INCLUDE_DIR}")
ELSE (EXISTS ${ISAM_INCLUDE_DIR})
  ISAM_REPORT_NOT_FOUND("Did not find ISAM Include Dir.")
  SET(ISAM_FOUND FALSE)
ENDIF (EXISTS ${ISAM_INCLUDE_DIR})

IF (EXISTS ${ISAM_LIBRARY})
  MESSAGE("Found ISAM Library in: ${ISAM_LIBRARY}")
ELSE (EXISTS ${ISAM_LIBRARY})
  ISAM_REPORT_NOT_FOUND("Did not find ISAM Library.")
  SET(ISAM_FOUND FALSE)
ENDIF (EXISTS ${ISAM_LIBRARY})
